# Introduction

Toelichting hier...

----

# Update instruction

To release a new ontology, follow these steps in succession

- clone the repo if it hasn't been done before `git clone git@gitlab.com:vngr-gbi/ontologie-inkomen.git`
- switch to the develop branch `git checkout develop`
- extract content of zipped specification into home dir of cloned repository (select Replace All where requested)
- remove `index.html`
- rename `Specificatie Ontologie-Inkomen -- vx.y.z.html` to `index.html`
- commit & push changes to the remote repository
    - `git add .`
    - `git commit -m "Updated to x.y.z version"`
    - `git push origin develop`
- create an annotated tag en push the tag to the remote repository
    - `git tag -a vx.y.z -m "release x.y.z"`
    - `git push origin --tags`
- create a merge pull request
